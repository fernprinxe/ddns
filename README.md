# cloudflare ddns shell

[![ddns](https://img.shields.io/badge/license-BSD3--clause%20license-brightgreen)](https://en.wikipedia.org/wiki/BSD_licenses#3-clause_license_(%22BSD_License_2.0%22,_%22Revised_BSD_License%22,_%22New_BSD_License%22,_or_%22Modified_BSD_License%22))

## edit

```
curl -o /usr/local/bin/ddns.sh https://gitlab.com/fernprinxe/ddns/-/raw/master/ddns.sh
vi /usr/local/bin/ddns.sh
```

## run

```
chmod +x /usr/local/bin/ddns.sh
/bin/bash /usr/local/bin/ddns.sh
```

## auto

```
curl -o /lib/systemd/system/ddns.service https://gitlab.com/fernprinxe/ddns/-/raw/master/ddns.service
curl -o /lib/systemd/system/ddns.timer https://gitlab.com/fernprinxe/ddns/-/raw/master/ddns.timer
systemctl enable ddns.timer
systemctl start ddns.timer
systemctl status ddns
```

## help

cloudflare: https://www.cloudflare.com/learning/dns/glossary/dynamic-dns  
yulewang: https://github.com/yulewang/cloudflare-api-v4-ddns

## issue

an error will occur in the first run under certain circumstances `no file, need ip.`, already testing.